<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\IngredientRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Length;

/**
 * @ORM\Entity(repositoryClass=IngredientRepository::class)
 */
#[ApiResource(
    collectionOperations: ['get'],
    itemOperations: ['get']
)]
class Ingredient
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[
        Groups(['write:Recipe', 'read:Recipe']),
        Length(min: 2, max: 20)
    ]
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[
        Groups(['write:Recipe', 'read:Recipe']),
        Length(min: 2, max: 20)
    ]
    private $isQuantifiedIn;

    /**
     * @ORM\ManyToOne(targetEntity=Recipe::class, inversedBy="ingredients")
     * @ORM\JoinColumn(nullable=false)
     */
    private $recipe;

    /**
     * @ORM\Column(type="integer")
     */
    #[
        Groups(['write:Recipe', 'read:Recipe']),
        Length(min: 2, max: 10)
    ]
    private $quantity;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsQuantifiedIn(): ?string
    {
        return $this->isQuantifiedIn;
    }

    public function setIsQuantifiedIn(string $isQuantifiedIn): self
    {
        $this->isQuantifiedIn = $isQuantifiedIn;

        return $this;
    }

    public function getRecipe(): ?Recipe
    {
        return $this->recipe;
    }

    public function setRecipe(?Recipe $recipe): self
    {
        $this->recipe = $recipe;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }
}
