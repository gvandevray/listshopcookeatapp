<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\RecipeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Valid;

/**
 * @ORM\Entity(repositoryClass=RecipeRepository::class)
 */
#[ApiResource(
    denormalizationContext: ['groups' => ['write:Recipe']],
    normalizationContext: ['groups' => ['read:Recipe']],
    collectionOperations: ['get', 'post'],
    itemOperations: ['get', 'put', 'delete']
)]
class Recipe
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    #[
        Groups(['write:Recipe', 'read:Recipe', 'read:Category', 'read:subCategory']),
        Length(min: 3, max: 50)
    ]
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=StepRecipe::class, mappedBy="recipe", orphanRemoval=true, cascade={"persist"})
     */
    #[
        Groups(['write:Recipe', 'read:Recipe']),
        Valid()
    ]
    private $stepPreparation;

    /**
     * @ORM\Column(type="integer")
     */
    #[
        Groups(['write:Recipe', 'read:Recipe']),
        Length(min: 1, max: 4)
    ]
    private $serving;

    /**
     * @ORM\Column(type="integer")
     */
    #[
        Groups(['write:Recipe', 'read:Recipe']),
        Length(min: 1, max: 4)
    ]
    private $timeToPrepare;

    /**
     * @ORM\Column(type="integer")
     */
    #[
        Groups(['write:Recipe', 'read:Recipe']),
        Length(min: 1, max: 4)
    ]
    private $timeToCook;

    /**
     * @ORM\ManyToMany(targetEntity=Category::class, inversedBy="recipes", cascade={"persist"})
     */
    #[
        Groups(['write:Recipe', 'read:Recipe']),
        Valid()
    ]
    private $categories;

    /**
     * @ORM\ManyToMany(targetEntity=SubCategory::class, inversedBy="recipes", cascade={"persist"})
     */
    #[
        Groups(['write:Recipe', 'read:Recipe']),
        Valid()
    ]
    private $subCategories;

    /**
     * @ORM\OneToMany(targetEntity=Picture::class, mappedBy="recipe")
     */
    #[Groups(['write:Recipe', 'read:Recipe'])]
    private $picture;

    /**
     * @ORM\Column(type="datetime")
     */
    #[Groups(['read:Recipe'])]
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="text")
     */
    #[Groups(['write:Recipe', 'read:Recipe'])]
    private $presentation;

    /**
     * @ORM\OneToMany(targetEntity=Ingredient::class, mappedBy="recipe", orphanRemoval=true, cascade={"persist"})
     */
    #[
        Groups(['write:Recipe', 'read:Recipe']),
        Valid()
    ]
    private $ingredients;

    /**
     * @ORM\ManyToOne(targetEntity=CostRecipe::class, inversedBy="recipes", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    #[
        Groups(['write:Recipe', 'read:Recipe']),
        Valid()
    ]
    private $cost;

    /**
     * @ORM\ManyToOne(targetEntity=Level::class, inversedBy="recipes", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    #[
        Groups(['write:Recipe', 'read:Recipe']),
        Valid()
    ]
    private $level;

    public function __construct()
    {
        $this->stepPreparation = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->subCategories = new ArrayCollection();
        $this->picture = new ArrayCollection();
        $this->ingredients = new ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|StepRecipe[]
     */
    public function getStepPreparation(): Collection
    {
        return $this->stepPreparation;
    }

    public function addStepPreparation(StepRecipe $stepPreparation): self
    {
        if (!$this->stepPreparation->contains($stepPreparation)) {
            $this->stepPreparation[] = $stepPreparation;
            $stepPreparation->setRecipe($this);
        }

        return $this;
    }

    public function removeStepPreparation(StepRecipe $stepPreparation): self
    {
        if ($this->stepPreparation->removeElement($stepPreparation)) {
            // set the owning side to null (unless already changed)
            if ($stepPreparation->getRecipe() === $this) {
                $stepPreparation->setRecipe(null);
            }
        }

        return $this;
    }

    public function getServing(): ?int
    {
        return $this->serving;
    }

    public function setServing(int $serving): self
    {
        $this->serving = $serving;

        return $this;
    }

    public function getTimeToPrepare(): ?int
    {
        return $this->timeToPrepare;
    }

    public function setTimeToPrepare(int $timeToPrepare): self
    {
        $this->timeToPrepare = $timeToPrepare;

        return $this;
    }

    public function getTimeToCook(): ?int
    {
        return $this->timeToCook;
    }

    public function setTimeToCook(int $timeToCook): self
    {
        $this->timeToCook = $timeToCook;

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        $this->categories->removeElement($category);

        return $this;
    }

    /**
     * @return Collection|SubCategory[]
     */
    public function getSubCategories(): Collection
    {
        return $this->subCategories;
    }

    public function addSubCategory(SubCategory $subCategory): self
    {
        if (!$this->subCategories->contains($subCategory)) {
            $this->subCategories[] = $subCategory;
        }

        return $this;
    }

    public function removeSubCategory(SubCategory $subCategory): self
    {
        $this->subCategories->removeElement($subCategory);

        return $this;
    }

    /**
     * @return Collection|Picture[]
     */
    public function getPicture(): Collection
    {
        return $this->picture;
    }

    public function addPicture(Picture $picture): self
    {
        if (!$this->picture->contains($picture)) {
            $this->picture[] = $picture;
            $picture->setRecipe($this);
        }

        return $this;
    }

    public function removePicture(Picture $picture): self
    {
        if ($this->picture->removeElement($picture)) {
            // set the owning side to null (unless already changed)
            if ($picture->getRecipe() === $this) {
                $picture->setRecipe(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getPresentation(): ?string
    {
        return $this->presentation;
    }

    public function setPresentation(string $presentation): self
    {
        $this->presentation = $presentation;

        return $this;
    }

    /**
     * @return Collection|Ingredient[]
     */
    public function getIngredients(): Collection
    {
        return $this->ingredients;
    }

    public function addIngredient(Ingredient $ingredient): self
    {
        if (!$this->ingredients->contains($ingredient)) {
            $this->ingredients[] = $ingredient;
            $ingredient->setRecipe($this);
        }

        return $this;
    }

    public function removeIngredient(Ingredient $ingredient): self
    {
        if ($this->ingredients->removeElement($ingredient)) {
            // set the owning side to null (unless already changed)
            if ($ingredient->getRecipe() === $this) {
                $ingredient->setRecipe(null);
            }
        }

        return $this;
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    public function getCost(): ?CostRecipe
    {
        return $this->cost;
    }

    public function setCost(?CostRecipe $cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    public function getLevel(): ?Level
    {
        return $this->level;
    }

    public function setLevel(?Level $level): self
    {
        $this->level = $level;

        return $this;
    }
}
