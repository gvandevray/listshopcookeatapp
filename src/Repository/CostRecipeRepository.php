<?php

namespace App\Repository;

use App\Entity\CostRecipe;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CostRecipe|null find($id, $lockMode = null, $lockVersion = null)
 * @method CostRecipe|null findOneBy(array $criteria, array $orderBy = null)
 * @method CostRecipe[]    findAll()
 * @method CostRecipe[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CostRecipeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CostRecipe::class);
    }

    // /**
    //  * @return CostRecipe[] Returns an array of CostRecipe objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CostRecipe
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
