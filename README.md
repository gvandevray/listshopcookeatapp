# ListShopCooK&EatApp

Génération d'une liste de courses à partir d'une liste de recettes

## Environnement de développement

### Pré-requis

* PHP 7.4
* Composer
* Symfony CLI
* Docker
* Docker-compose

Vous pouvez vérifier les pré-requis (sauf Docker et Docker-compose) avec la commande suivante (de la CLI symfony) :

```bash
symfony check:requirements
```
### Lancer l'environnement de dévoloppement

```bash
composer install
docker-compose up -d
```
```bash2
symfony serve
```
### Documentation de l'API

Serveur local ouvert sur port 8000
127.0.0.1:8000/api