<?php

namespace App\Tests\Entity;

use App\Entity\Picture;
use PHPUnit\Framework\TestCase;

class PictureTest extends TestCase
{
    public function testIsTrue(): void
    {
        $picture = new Picture();
        $picture->setName('picture.png');

        $this->assertTrue($picture->getName() === 'picture.png');
    }

    public function testIsFalse()
    {
        $picture = new Picture();
        $picture->setName('picture.png');

        $this->assertFalse($picture->getName() === 'otherpicture.png');
    }

    public function testIsEmpty()
    {

        $picture = new Picture();

        $this->assertEmpty($picture->getName());
    }
}
