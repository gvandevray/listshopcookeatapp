<?php

namespace App\Tests\Entity;

use App\Entity\Ingredient;
use PHPUnit\Framework\TestCase;

class IngredientTest extends TestCase
{
    public function testIsTrue(): void
    {
        $ingredient = new Ingredient();
        $ingredient->setName('sucre');
        $ingredient->setIsQuantifiedIn('mg');

        $this->assertTrue($ingredient->getName() === 'sucre');
        $this->assertTrue($ingredient->getIsQuantifiedIn() === 'mg');
    }

    public function testIsFalse(): void
    {
        $ingredient = new Ingredient();
        $ingredient->setName('sucre');
        $ingredient->setIsQuantifiedIn('mg');

        $this->assertFalse($ingredient->getName() === 'sel');
        $this->assertFalse($ingredient->getIsQuantifiedIn() === 'ml');
    }

    public function testIsEmpty(): void
    {
        $ingredient = new Ingredient();

        $this->assertEmpty($ingredient->getName());
    }
}
