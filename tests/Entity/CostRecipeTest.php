<?php

namespace App\Tests\Entity;

use App\Entity\CostRecipe;
use PHPUnit\Framework\TestCase;

class CostRecipeTest extends TestCase
{
    public function testIsTrue(): void
    {
        $cost = new CostRecipe();
        $cost->setDegree(1);

        $this->assertTrue($cost->getDegree() === 1);
    }

    public function testIsFalse(): void
    {
        $cost = new CostRecipe();
        $cost->setDegree(1);

        $this->assertFalse($cost->getDegree() === 2);
    }

    public function testIsEmpty(): void
    {
        $cost = new CostRecipe();
        $this->assertEmpty($cost->getDegree());
    }
}
