<?php

namespace App\Tests\Entity;

use App\Entity\StepRecipe;
use PHPUnit\Framework\TestCase;

class StepRecipeTest extends TestCase
{
    public function testIsTrue(): void
    {
        $stepRecipe = new StepRecipe();
        $stepRecipe->setStep(1)
            ->setDescription('une description');

        $this->assertTrue($stepRecipe->getStep() === 1);
        $this->assertTrue($stepRecipe->getDescription() === 'une description');
    }

    public function testIsFalse(): void
    {
        $stepRecipe = new StepRecipe();
        $stepRecipe->setStep(1)
            ->setDescription('une description');

        $this->assertFalse($stepRecipe->getStep() === 2);
        $this->assertFalse($stepRecipe->getDescription() === 'une autre description');
    }

    public function testIsEmpty(): void
    {
        $stepRecipe = new StepRecipe();

        $this->assertEmpty($stepRecipe->getStep());
        $this->assertEmpty($stepRecipe->getDescription());
    }
}
