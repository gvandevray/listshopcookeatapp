<?php

namespace App\Tests\Entity;

use App\Entity\Category;
use App\Entity\SubCategory;
use PHPUnit\Framework\TestCase;

class CategoryTest extends TestCase
{
    public function testIsTrue(): void
    {

        $category = new Category();
        $category->setName('Lunch');

        $this->assertTrue($category->getName() === 'Lunch');
    }

    public function testIsFalse(): void
    {
        $category = new Category();
        $category->setName('Lunch');

        $this->assertFalse($category->getName() === 'Breakfast');
    }

    public function testIsEmpty(): void
    {
        $category = new Category();

        $this->assertEmpty($category->getName());
    }
}
