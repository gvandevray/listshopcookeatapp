<?php

namespace App\Tests\Entity;

use App\Entity\SubCategory;
use PHPUnit\Framework\TestCase;

class SubCategoryTest extends TestCase
{
    public function testIsTrue(): void
    {
        $subCategory1 = new SubCategory();
        $subCategory1->setName('Japanese');

        $this->assertTrue($subCategory1->getName() === 'Japanese');
    }

    public function testIsFalse()
    {

        $subCategory = new SubCategory();
        $subCategory->setName('Japanese');

        $this->assertFalse($subCategory->getName() === 'Turkish');
    }

    public function testIsEmpty()
    {

        $subCategory = new SubCategory();

        $this->assertEmpty($subCategory->getName());
    }
}
