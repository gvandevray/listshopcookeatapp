<?php

namespace App\Tests;

use App\Entity\Category;
use App\Entity\CostRecipe;
use App\Entity\Ingredient;
use App\Entity\Level;
use App\Entity\Recipe;
use App\Entity\StepRecipe;
use App\Entity\SubCategory;
use PHPUnit\Framework\TestCase;
use DateTime;

class RecipeTest extends TestCase
{
    public function testIsTrue(): void
    {
        $recipe = new Recipe();

        $newDate = new DateTime();

        $ingredient = new Ingredient();
        $ingredient->setIsQuantifiedIn('g')
            ->setName('sucre')
            ->setQuantity(10);

        $stepPreparation = new StepRecipe();
        $stepPreparation->setStep(1)
            ->setDescription('une description')
            ->setRecipe($recipe);
        $otherStepPreparation = new StepRecipe();
        $otherStepPreparation->setStep(2)
            ->setDescription('une autre description')
            ->setRecipe($recipe);

        $category = new Category();
        $category->setName('Lunch')
            ->addRecipe($recipe);

        $subCategory = new SubCategory();
        $subCategory->setName('Japonais')
            ->addRecipe($recipe);

        $cost = new CostRecipe();
        $cost->setDegree(1);

        $level = new Level();
        $level->setLevel(1);


        $recipe->setName('Frites')
            ->addStepPreparation($stepPreparation)
            ->addStepPreparation($otherStepPreparation)
            ->addIngredient($ingredient)
            ->addCategory($category)
            ->addSubCategory($subCategory)
            ->setServing(4)
            ->setTimeToPrepare(10)
            ->setTimeToCook(60)
            ->setCreatedAt($newDate)
            ->setPresentation('une presentation de la recette')
            ->setCost($cost)
            ->setLevel($level);

        $this->assertTrue($recipe->getName() === 'Frites');
        $this->assertTrue($recipe->getIngredients()[0] === $ingredient);
        $this->assertTrue($recipe->getStepPreparation()[0] === $stepPreparation);
        $this->assertTrue($recipe->getStepPreparation()[1] === $otherStepPreparation);
        $this->assertTrue($recipe->getCategories()[0] === $category);
        $this->assertTrue($recipe->getSubCategories()[0] === $subCategory);
        $this->assertTrue($recipe->getServing() === 4);
        $this->assertTrue($recipe->getTimeToPrepare() === 10);
        $this->assertTrue($recipe->getTimeToCook() === 60);
        $this->assertTrue($recipe->getCreatedAt() === $newDate);
        $this->assertTrue($recipe->getPresentation() === 'une presentation de la recette');
        $this->assertTrue($recipe->getCost() === $cost);
        $this->assertTrue($recipe->getLevel() === $level);
    }

    public function testIsFalse(): void
    {
        $recipe = new Recipe();

        $newDate = new DateTime();
        $newDate2 = new DateTime();

        $ingredient = new Ingredient();
        $ingredient->setIsQuantifiedIn('g')
            ->setName('sucre')
            ->setQuantity(10);

        $ingredient2 = new Ingredient();
        $ingredient2->setIsQuantifiedIn('g')
            ->setName('sel')
            ->setQuantity(10);

        $stepPreparation = new StepRecipe();
        $stepPreparation->setStep(1)
            ->setDescription('une description')
            ->setRecipe($recipe);
        $otherStepPreparation = new StepRecipe();
        $otherStepPreparation->setStep(2)
            ->setDescription('une autre description')
            ->setRecipe($recipe);

        $category = new Category();
        $category->setName('Lunch')
            ->addRecipe($recipe);

        $otherCategory = new Category();
        $otherCategory->setName('Breakfast')
            ->addRecipe($recipe);

        $subCategory = new SubCategory();
        $subCategory->setName('Japonais')
            ->addRecipe($recipe);

        $otherSubCategory = new SubCategory();
        $otherSubCategory->setName('Libanais')
            ->addRecipe($recipe);

        $cost = new CostRecipe();
        $cost->setDegree(1);

        $cost2 = new CostRecipe();
        $cost2->setDegree(2);

        $level = new Level();
        $level->setLevel(1);

        $level2 = new Level();
        $level2->setLevel(2);


        $recipe->setName('Frites')
            ->addStepPreparation($stepPreparation)
            ->addStepPreparation($otherStepPreparation)
            ->addIngredient($ingredient)
            ->addCategory($category)
            ->addSubCategory($subCategory)
            ->addIngredient($ingredient)
            ->setServing(4)
            ->setTimeToPrepare(10)
            ->setTimeToCook(60)
            ->setCreatedAt($newDate)
            ->setPresentation('une presentation de la recette')
            ->setCost($cost)
            ->setLevel($level);

        $this->assertFalse($recipe->getName() === 'Potatoes');
        $this->assertFalse($recipe->getStepPreparation()[1] === $stepPreparation);
        $this->assertFalse($recipe->getStepPreparation()[1]->getStep() === 1);
        $this->assertFalse($recipe->getStepPreparation()[0] === $otherStepPreparation);
        $this->assertFalse($recipe->getCategories()[0] === $otherCategory);
        $this->assertFalse($recipe->getSubCategories()[0] === $otherSubCategory);
        $this->assertFalse($recipe->getServing() === 40);
        $this->assertFalse($recipe->getTimeToPrepare() === 100);
        $this->assertFalse($recipe->getTimeToCook() === 600);
        $this->assertFalse($recipe->getCreatedAt() === $newDate2);
        $this->assertFalse($recipe->getPresentation() === 'une autre presentation de la recette');
        $this->assertFalse($recipe->getCost() === $cost2);
        $this->assertFalse($recipe->getLevel() === $level2);
        $this->assertFalse($recipe->getIngredients()[0] === $ingredient2);
    }

    public function testIsEmpty(): void
    {
        $recipe = new Recipe();
        $this->assertEmpty($recipe->getName());
        $this->assertEmpty($recipe->getStepPreparation());
        $this->assertEmpty($recipe->getStepPreparation());
        $this->assertEmpty($recipe->getCategories());
        $this->assertEmpty($recipe->getSubCategories());
        $this->assertEmpty($recipe->getServing());
        $this->assertEmpty($recipe->getTimeToPrepare());
        $this->assertEmpty($recipe->getTimeToCook());
        $this->assertEmpty($recipe->getPresentation());
        $this->assertEmpty($recipe->getCost());
        $this->assertEmpty($recipe->getLevel());

        $this->assertNotEmpty($recipe->getCreatedAt());
        $this->assertNotEmpty($recipe->getUpdatedAt());
    }
}
