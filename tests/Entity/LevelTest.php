<?php

namespace App\Tests\Entity;

use App\Entity\Level;
use PHPUnit\Framework\TestCase;

class LevelTest extends TestCase
{
    public function testIsTrue(): void
    {
        $level = new Level();
        $level->setLevel(1);

        $this->assertTrue($level->getLevel() === 1);
    }

    public function testIsFalse(): void
    {
        $level = new Level();
        $level->setLevel(1);

        $this->assertFalse($level->getLevel() === 2);
    }

    public function testIsEmpty(): void
    {
        $level = new Level();
        $this->assertEmpty($level->getLevel());
    }
}
